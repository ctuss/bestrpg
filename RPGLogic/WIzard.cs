﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGLogic
{
    [Serializable]
    public class Wizard : Character
    {
        public Wizard(string name) : base(name, "Mana")
        {
            Hp = 150;
            Resource = 300;
            ArmorRating = 50;
        }

        public override string Attack()
        {
            return "Wizard attack!";
        }

        public override string Move()
        {
            return "Wizard move!";
        }
    }
}
