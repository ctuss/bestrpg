﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGLogic
{
    [Serializable]
    public abstract class Character
    {
        public string Name { get; set; }
        public int Hp { get; set; }
        public int Resource { get; set; }
        public string ResourceType { get; set; }
        public int ArmorRating { get; set; }
        public Character(string name, string resourceType)
        {
            Name = name;
            ResourceType = resourceType;
        }

        public abstract string Attack();
        public abstract string Move();
    }
}
