﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGLogic
{
    [Serializable]
    public class Warrior : Character
    {

        public Warrior(string name) : base(name, "Rage")
        {
            Hp = 300;
            Resource = 100;
            ArmorRating = 200;
        }
        public override string Attack()
        {
            return "Warrior attack!";
        }

        public override string Move()
        {
            return "Warrior move!";
        }
    }
}
