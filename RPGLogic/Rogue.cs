﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGLogic
{
    [Serializable]
    public class Rogue : Character
    {
        public Rogue(string name) : base(name, "Energy")
        {
            Hp = 225;
            Resource = 100;
            ArmorRating = 125;
        }
        public override string Attack()
        {
            return "Rogue attack!";
        }

        public override string Move()
        {
            return "Rogue move!";
        }
    }
}
