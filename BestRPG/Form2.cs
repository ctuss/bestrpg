﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RPGCharacterGenerator;

namespace BestRPG
{
    public partial class Form2 : Form
    {
        Character character;
        public Form2()
        {
            InitializeComponent();
            //this.Character = character;
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            characterName.ReadOnly = false;
            characterName.Text = character.Name;
            characterName.ReadOnly = true;
            
            inputHp.ReadOnly = false;
            inputHp.Text = character.HP.ToString();
            inputHp.ReadOnly = true;

            textBoxResourceType.ReadOnly = false;
            textBoxResourceType.Text = character.ResourceType.ToString();
            textBoxResourceType.ReadOnly = true;

            inputResource.ReadOnly = false;
            inputResource.Text = character.Resource.ToString();
            inputResource.ReadOnly = true;

            inputArmorRating.ReadOnly = false;
            inputArmorRating.Text = character.ArmorRating.ToString();
            inputArmorRating.ReadOnly = true;
        }

        private void buttonGoBack_Click(object sender, EventArgs e)
        {
            inputAction.ReadOnly = false;
            inputAction.Text = "";
            inputAction.ReadOnly = true;

            FormState.PreviousPage.Show();
            this.Hide();
            FormState.PreviousPage = this;
        }

        public void DisplayCharacterInfo(Character character)
        {
            this.character = character;

            characterName.ReadOnly = false;
            characterName.Text = character.Name;
            characterName.ReadOnly = true;

            inputHp.ReadOnly = false;
            inputHp.Text = character.HP.ToString();
            inputHp.ReadOnly = true;

            textBoxResourceType.ReadOnly = false;
            textBoxResourceType.Text = character.ResourceType.ToString();
            textBoxResourceType.ReadOnly = true;

            inputResource.ReadOnly = false;
            inputResource.Text = character.Resource.ToString();
            inputResource.ReadOnly = true;

            inputArmorRating.ReadOnly = false;
            inputArmorRating.Text = character.ArmorRating.ToString();
            inputArmorRating.ReadOnly = true;
        }

        private void buttonAttack_Click(object sender, EventArgs e)
        {
            inputAction.ReadOnly = false;
            inputAction.Text = character.Attack();
            inputAction.ReadOnly = true;
        }

        private void buttonMove_Click(object sender, EventArgs e)
        {
            inputAction.ReadOnly = false;
            inputAction.Text = character.Move();
            inputAction.ReadOnly = true;
        }
    }
}
