﻿namespace BestRPG
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.inputName = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBoxMana = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.radioButtonWarrior = new System.Windows.Forms.RadioButton();
            this.radioButtonRogue = new System.Windows.Forms.RadioButton();
            this.radioButtonWizard = new System.Windows.Forms.RadioButton();
            this.buttonCreateCharacter = new System.Windows.Forms.Button();
            this.listCharacterView = new System.Windows.Forms.ListView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.inputArmorRating = new System.Windows.Forms.TextBox();
            this.inputResource = new System.Windows.Forms.TextBox();
            this.inputHp = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // inputName
            // 
            this.inputName.Location = new System.Drawing.Point(24, 158);
            this.inputName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.inputName.Name = "inputName";
            this.inputName.Size = new System.Drawing.Size(153, 22);
            this.inputName.TabIndex = 4;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(24, 133);
            this.textBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(153, 22);
            this.textBox2.TabIndex = 5;
            this.textBox2.Text = "Name";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(24, 184);
            this.textBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(89, 22);
            this.textBox3.TabIndex = 6;
            this.textBox3.Text = "HP";
            // 
            // textBoxMana
            // 
            this.textBoxMana.Location = new System.Drawing.Point(24, 210);
            this.textBoxMana.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxMana.Name = "textBoxMana";
            this.textBoxMana.ReadOnly = true;
            this.textBoxMana.Size = new System.Drawing.Size(89, 22);
            this.textBoxMana.TabIndex = 8;
            this.textBoxMana.Text = "Resource";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(24, 235);
            this.textBox5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox5.Name = "textBox5";
            this.textBox5.ReadOnly = true;
            this.textBox5.Size = new System.Drawing.Size(89, 22);
            this.textBox5.TabIndex = 10;
            this.textBox5.Text = "Armor Rating";
            // 
            // radioButtonWarrior
            // 
            this.radioButtonWarrior.AutoSize = true;
            this.radioButtonWarrior.Location = new System.Drawing.Point(24, 38);
            this.radioButtonWarrior.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radioButtonWarrior.Name = "radioButtonWarrior";
            this.radioButtonWarrior.Size = new System.Drawing.Size(76, 21);
            this.radioButtonWarrior.TabIndex = 12;
            this.radioButtonWarrior.TabStop = true;
            this.radioButtonWarrior.Text = "Warrior";
            this.radioButtonWarrior.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radioButtonWarrior.UseVisualStyleBackColor = true;
            this.radioButtonWarrior.CheckedChanged += new System.EventHandler(this.radioButtonWarrior_CheckedChanged);
            // 
            // radioButtonRogue
            // 
            this.radioButtonRogue.AutoSize = true;
            this.radioButtonRogue.Location = new System.Drawing.Point(24, 70);
            this.radioButtonRogue.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radioButtonRogue.Name = "radioButtonRogue";
            this.radioButtonRogue.Size = new System.Drawing.Size(71, 21);
            this.radioButtonRogue.TabIndex = 13;
            this.radioButtonRogue.TabStop = true;
            this.radioButtonRogue.Text = "Rogue";
            this.radioButtonRogue.UseVisualStyleBackColor = true;
            this.radioButtonRogue.CheckedChanged += new System.EventHandler(this.radioButtonRogue_CheckedChanged);
            // 
            // radioButtonWizard
            // 
            this.radioButtonWizard.AutoSize = true;
            this.radioButtonWizard.Location = new System.Drawing.Point(24, 103);
            this.radioButtonWizard.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radioButtonWizard.Name = "radioButtonWizard";
            this.radioButtonWizard.Size = new System.Drawing.Size(73, 21);
            this.radioButtonWizard.TabIndex = 14;
            this.radioButtonWizard.TabStop = true;
            this.radioButtonWizard.Text = "Wizard";
            this.radioButtonWizard.UseVisualStyleBackColor = true;
            this.radioButtonWizard.CheckedChanged += new System.EventHandler(this.radioButtonWizard_CheckedChanged);
            // 
            // buttonCreateCharacter
            // 
            this.buttonCreateCharacter.Location = new System.Drawing.Point(24, 289);
            this.buttonCreateCharacter.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonCreateCharacter.Name = "buttonCreateCharacter";
            this.buttonCreateCharacter.Size = new System.Drawing.Size(153, 34);
            this.buttonCreateCharacter.TabIndex = 15;
            this.buttonCreateCharacter.Text = "Create Character";
            this.buttonCreateCharacter.UseVisualStyleBackColor = true;
            this.buttonCreateCharacter.Click += new System.EventHandler(this.buttonCreateCharacter_Click);
            // 
            // listCharacterView
            // 
            this.listCharacterView.HideSelection = false;
            this.listCharacterView.Location = new System.Drawing.Point(258, 45);
            this.listCharacterView.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.listCharacterView.Name = "listCharacterView";
            this.listCharacterView.Size = new System.Drawing.Size(235, 219);
            this.listCharacterView.TabIndex = 16;
            this.listCharacterView.UseCompatibleStateImageBehavior = false;
            this.listCharacterView.View = System.Windows.Forms.View.List;
            this.listCharacterView.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.inputArmorRating);
            this.groupBox1.Controls.Add(this.inputResource);
            this.groupBox1.Controls.Add(this.inputHp);
            this.groupBox1.Controls.Add(this.buttonCreateCharacter);
            this.groupBox1.Controls.Add(this.radioButtonWizard);
            this.groupBox1.Controls.Add(this.radioButtonRogue);
            this.groupBox1.Controls.Add(this.radioButtonWarrior);
            this.groupBox1.Controls.Add(this.textBox5);
            this.groupBox1.Controls.Add(this.textBoxMana);
            this.groupBox1.Controls.Add(this.textBox3);
            this.groupBox1.Controls.Add(this.textBox2);
            this.groupBox1.Controls.Add(this.inputName);
            this.groupBox1.Location = new System.Drawing.Point(8, 10);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(211, 339);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Character Creation";
            // 
            // inputArmorRating
            // 
            this.inputArmorRating.Location = new System.Drawing.Point(120, 235);
            this.inputArmorRating.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.inputArmorRating.Name = "inputArmorRating";
            this.inputArmorRating.ReadOnly = true;
            this.inputArmorRating.Size = new System.Drawing.Size(57, 22);
            this.inputArmorRating.TabIndex = 18;
            // 
            // inputResource
            // 
            this.inputResource.Location = new System.Drawing.Point(120, 210);
            this.inputResource.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.inputResource.Name = "inputResource";
            this.inputResource.ReadOnly = true;
            this.inputResource.Size = new System.Drawing.Size(57, 22);
            this.inputResource.TabIndex = 17;
            // 
            // inputHp
            // 
            this.inputHp.Location = new System.Drawing.Point(120, 185);
            this.inputHp.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.inputHp.Name = "inputHp";
            this.inputHp.ReadOnly = true;
            this.inputHp.Size = new System.Drawing.Size(57, 22);
            this.inputHp.TabIndex = 16;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(709, 358);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.listCharacterView);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TextBox inputName;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBoxMana;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.RadioButton radioButtonWarrior;
        private System.Windows.Forms.RadioButton radioButtonRogue;
        private System.Windows.Forms.RadioButton radioButtonWizard;
        private System.Windows.Forms.Button buttonCreateCharacter;
        private System.Windows.Forms.ListView listCharacterView;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox inputArmorRating;
        private System.Windows.Forms.TextBox inputResource;
        private System.Windows.Forms.TextBox inputHp;
    }
}

