﻿namespace BestRPG
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.inputArmorRating = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.inputResource = new System.Windows.Forms.TextBox();
            this.textBoxResourceType = new System.Windows.Forms.TextBox();
            this.inputHp = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.characterName = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.buttonGoBack = new System.Windows.Forms.Button();
            this.buttonMove = new System.Windows.Forms.Button();
            this.buttonAttack = new System.Windows.Forms.Button();
            this.inputAction = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonMove);
            this.groupBox1.Controls.Add(this.buttonAttack);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.characterName);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Location = new System.Drawing.Point(22, 20);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(253, 250);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Character Profile";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.inputArmorRating);
            this.groupBox2.Controls.Add(this.textBox6);
            this.groupBox2.Controls.Add(this.inputResource);
            this.groupBox2.Controls.Add(this.textBoxResourceType);
            this.groupBox2.Controls.Add(this.inputHp);
            this.groupBox2.Controls.Add(this.textBox2);
            this.groupBox2.Location = new System.Drawing.Point(23, 87);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Size = new System.Drawing.Size(210, 103);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Stats";
            // 
            // inputArmorRating
            // 
            this.inputArmorRating.Location = new System.Drawing.Point(101, 72);
            this.inputArmorRating.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.inputArmorRating.Name = "inputArmorRating";
            this.inputArmorRating.ReadOnly = true;
            this.inputArmorRating.Size = new System.Drawing.Size(104, 22);
            this.inputArmorRating.TabIndex = 5;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(6, 73);
            this.textBox6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox6.Name = "textBox6";
            this.textBox6.ReadOnly = true;
            this.textBox6.Size = new System.Drawing.Size(89, 22);
            this.textBox6.TabIndex = 4;
            this.textBox6.Text = "Armor Rating";
            // 
            // inputResource
            // 
            this.inputResource.Location = new System.Drawing.Point(101, 46);
            this.inputResource.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.inputResource.Name = "inputResource";
            this.inputResource.ReadOnly = true;
            this.inputResource.Size = new System.Drawing.Size(104, 22);
            this.inputResource.TabIndex = 3;
            // 
            // textBoxResourceType
            // 
            this.textBoxResourceType.Location = new System.Drawing.Point(5, 47);
            this.textBoxResourceType.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxResourceType.Name = "textBoxResourceType";
            this.textBoxResourceType.ReadOnly = true;
            this.textBoxResourceType.Size = new System.Drawing.Size(89, 22);
            this.textBoxResourceType.TabIndex = 2;
            this.textBoxResourceType.Text = "Resource";
            // 
            // inputHp
            // 
            this.inputHp.Location = new System.Drawing.Point(100, 19);
            this.inputHp.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.inputHp.Name = "inputHp";
            this.inputHp.ReadOnly = true;
            this.inputHp.Size = new System.Drawing.Size(104, 22);
            this.inputHp.TabIndex = 1;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(5, 20);
            this.textBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(89, 22);
            this.textBox2.TabIndex = 0;
            this.textBox2.Text = "HP";
            // 
            // characterName
            // 
            this.characterName.Location = new System.Drawing.Point(124, 45);
            this.characterName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.characterName.Name = "characterName";
            this.characterName.ReadOnly = true;
            this.characterName.Size = new System.Drawing.Size(104, 22);
            this.characterName.TabIndex = 1;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(28, 45);
            this.textBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(89, 22);
            this.textBox1.TabIndex = 0;
            this.textBox1.Text = "Name:";
            // 
            // buttonGoBack
            // 
            this.buttonGoBack.Location = new System.Drawing.Point(51, 291);
            this.buttonGoBack.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonGoBack.Name = "buttonGoBack";
            this.buttonGoBack.Size = new System.Drawing.Size(67, 30);
            this.buttonGoBack.TabIndex = 1;
            this.buttonGoBack.Text = "Back";
            this.buttonGoBack.UseVisualStyleBackColor = true;
            this.buttonGoBack.Click += new System.EventHandler(this.buttonGoBack_Click);
            // 
            // buttonMove
            // 
            this.buttonMove.Location = new System.Drawing.Point(138, 204);
            this.buttonMove.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonMove.Name = "buttonMove";
            this.buttonMove.Size = new System.Drawing.Size(67, 30);
            this.buttonMove.TabIndex = 2;
            this.buttonMove.Text = "Move";
            this.buttonMove.UseVisualStyleBackColor = true;
            this.buttonMove.Click += new System.EventHandler(this.buttonMove_Click);
            // 
            // buttonAttack
            // 
            this.buttonAttack.Location = new System.Drawing.Point(40, 204);
            this.buttonAttack.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonAttack.Name = "buttonAttack";
            this.buttonAttack.Size = new System.Drawing.Size(67, 30);
            this.buttonAttack.TabIndex = 3;
            this.buttonAttack.Text = "Attack";
            this.buttonAttack.UseVisualStyleBackColor = true;
            this.buttonAttack.Click += new System.EventHandler(this.buttonAttack_Click);
            // 
            // inputAction
            // 
            this.inputAction.Location = new System.Drawing.Point(321, 224);
            this.inputAction.Name = "inputAction";
            this.inputAction.ReadOnly = true;
            this.inputAction.Size = new System.Drawing.Size(152, 22);
            this.inputAction.TabIndex = 2;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(711, 360);
            this.Controls.Add(this.inputAction);
            this.Controls.Add(this.buttonGoBack);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Form2";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox inputArmorRating;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox inputResource;
        private System.Windows.Forms.TextBox textBoxResourceType;
        private System.Windows.Forms.TextBox inputHp;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox characterName;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button buttonGoBack;
        private System.Windows.Forms.Button buttonMove;
        private System.Windows.Forms.Button buttonAttack;
        private System.Windows.Forms.TextBox inputAction;
    }
}