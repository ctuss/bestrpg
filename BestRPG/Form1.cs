﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using RPGCharacterGenerator;
using System.Data.SqlClient;

namespace BestRPG
{
    [Serializable]
    public partial class Form1 : Form
    {
        List<Character> characterList = new List<Character>();
        string name;
        Form2 displayInfoForm;

        public Form1()
        {
            InitializeComponent();
            displayInfoForm = new Form2();
        }

        private void radioButtonWarrior_CheckedChanged(object sender, EventArgs e)
        {
            textBoxMana.ReadOnly = false;
            textBoxMana.Text = "Rage";
            textBoxMana.ReadOnly = true;

            inputHp.ReadOnly = false;
            inputHp.Text = "300";
            inputHp.ReadOnly = true;

            inputResource.ReadOnly = false;
            inputResource.Text = "100";
            inputResource.ReadOnly = true;

            inputArmorRating.ReadOnly = false;
            inputArmorRating.Text = "150";
            inputArmorRating.ReadOnly = true;
        }

        private void radioButtonRogue_CheckedChanged(object sender, EventArgs e)
        {
            textBoxMana.ReadOnly = false;
            textBoxMana.Text = "Energy";
            textBoxMana.ReadOnly = true;

            inputHp.ReadOnly = false;
            inputHp.Text = "225";
            inputHp.ReadOnly = true;

            inputResource.ReadOnly = false;
            inputResource.Text = "100";
            inputResource.ReadOnly = true;

            inputArmorRating.ReadOnly = false;
            inputArmorRating.Text = "100";
            inputArmorRating.ReadOnly = true;
        }

        private void radioButtonWizard_CheckedChanged(object sender, EventArgs e)
        {
            textBoxMana.ReadOnly = false;
            textBoxMana.Text = "Mana";
            textBoxMana.ReadOnly = true;

            inputHp.ReadOnly = false;
            inputHp.Text = "150";
            inputHp.ReadOnly = true;

            inputResource.ReadOnly = false;
            inputResource.Text = "300";
            inputResource.ReadOnly = true;

            inputArmorRating.ReadOnly = false;
            inputArmorRating.Text = "75";
            inputArmorRating.ReadOnly = true;
        }

        private void buttonCreateCharacter_Click(object sender, EventArgs e)
        {
            name = inputName.Text;
            Character character = null;

            if (radioButtonWarrior.Checked)
            {
                character = new Warrior(name, 300, 150, 100);
                characterList.Add(character);
                AddObjectToDatabase(character);
                WriteObjectToFile(characterList);
                PopulateListView();
            }
            else if (radioButtonRogue.Checked)
            {
                character = new Rogue(name, 225, 100, 0, 100);
                characterList.Add(character);
                AddObjectToDatabase(character);
                WriteObjectToFile(characterList);
                PopulateListView();
            }
            else if (radioButtonWizard.Checked)
            {
                character = new Wizard(name, 150, 75, 300);
                characterList.Add(character);
                AddObjectToDatabase(character);
                WriteObjectToFile(characterList);
                PopulateListView();
            }
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int i = listCharacterView.FocusedItem.Index;
            DisplayCharacterInfo(characterList[i]);
        }

        //Populate ListView with characters
        private void PopulateListView()
        {
            //Clears views
            listCharacterView.Clear();

            //Adds all characters to view
            foreach (Character character in characterList)
            {
                ListViewItem lvi = new ListViewItem("Name: " + character.Name + " Class: " + character.GetType().Name);
                listCharacterView.Items.Add(lvi);
            }
        }

        private void WriteObjectToFile(List<Character> list)
        {
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(@"./character", FileMode.Create, FileAccess.Write);
            formatter.Serialize(stream, list);
            stream.Close();
        }
   
        private void PopulateListFromFile()
        {
            if (File.Exists(@"./character"))
            {
                IFormatter formatter = new BinaryFormatter();
                Stream stream = new FileStream("./character", FileMode.Open, FileAccess.Read);
                if (stream.Length != 0)
                {
                    characterList = (List<Character>)formatter.Deserialize(stream);
                }
            }
        }

        private void DisplayCharacterInfo(Character character)
        {
            displayInfoForm.DisplayCharacterInfo(character);
            displayInfoForm.Show();
            this.Hide();
            FormState.PreviousPage = this;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            PopulateListFromFile();
            PopulateListView();
        }

        private void AddObjectToDatabase(Character character)
        {
            string connectionString = null;
            SqlConnection cnn;

            connectionString = "Server = localhost\\SQLEXPRESS; Database = RPG_Characters; Trusted_Connection = true;";
            cnn = new SqlConnection(connectionString);

            try
            {
                cnn.Open();
                SqlCommand sql = new SqlCommand("INSERT INTO Characters VALUES('" +
                character.Name + "','" + character.HP + "','" + character.ArmorRating + "','" + character.Resource + "','" + character.ResourceType + "');", cnn);
                sql.ExecuteNonQuery();
                sql.Dispose();
                cnn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Cant connect to DB " + ex.Message);
                cnn.Close();
            }
        }
    }

    public static class FormState
    {
        public static Form PreviousPage;
    }
}