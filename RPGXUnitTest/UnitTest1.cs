using System;
using Xunit;
using RPGLogic;

namespace RPGXUnitTest
{
    public class UnitTest1
    {
        [Fact]
        public void TestSubTypeOfCharacter()
        {
            //arrange
            Character warrior = new Warrior("John");
            Character wizard = new Wizard("Jane");
            Character rogue = new Rogue("James");

            //act

            //assert
            Assert.Equal("Warrior", warrior.GetType().Name);
            Assert.Equal("Rogue", rogue.GetType().Name);
            Assert.Equal("Wizard", wizard.GetType().Name);
        }

        [Fact]
        public void TestCharacterAttack()
        {
            Character warrior = new Warrior("John");
            Character wizard = new Wizard("Jane");
            Character rogue = new Rogue("James");

            Assert.Equal("Warrior attack!", warrior.Attack());
            Assert.Equal("Wizard attack!", wizard.Attack());
            Assert.Equal("Rogue attack!", rogue.Attack());
        }

        [Fact]
        public void TestCharacterMove()
        {
            Character warrior = new Warrior("John");
            Character wizard = new Wizard("Jane");
            Character rogue = new Rogue("James");

            Assert.Equal("Warrior move!", warrior.Move());
            Assert.Equal("Wizard move!", wizard.Move());
            Assert.Equal("Rogue move!", rogue.Move());
        }
    }
}
